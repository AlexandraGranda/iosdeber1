//
//  ViewController.swift
//  Deber1
//
//  Created by Alexandra Granda on 24/5/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var nombreTextField: UITextField!
    @IBOutlet weak var apellidoTextField: UITextField!
    @IBOutlet weak var telefonoTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func ingresarButton(_ sender: Any) {
        let nombre = nombreTextField.text ?? "nombre"
        let apellido = apellidoTextField.text ?? "apellido"
        let telefono = telefonoTextField.text ?? "123456789"
     
        //print(locationManager.location?.coordinate)
			if validarTelefono(texto: telefono) {
				let bm = BackendManager()
				
				bm.insertarPersona(nombre: nombre, apellido: apellido, telefono: telefono)
				
				nombreTextField.text = "";
				apellidoTextField.text = "";
				telefonoTextField.text = "";
			}
			else {
				let alert = UIAlertController(title: "Datos inválidos", message: "El número ingresado no es válido", preferredStyle: UIAlertControllerStyle.alert)
				alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
				self.present(alert, animated: true, completion: nil)
			}
    }
	

    @IBAction func listarUsuarios(_ sender: Any) {
        performSegue(withIdentifier: "toUserListSegue", sender: self)
    }
	
	func validarTelefono(texto: String) -> Bool{
		let num = Int(texto)
		if num != nil {
			print("Valid Integer")
			return true
		}
		else {
			print("Not Valid Integer")
			return false
		}
	}

}

