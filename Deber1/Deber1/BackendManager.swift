//
//  BackendManager.swift
//  Deber1
//
//  Created by Alexandra Granda on 24/5/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import Foundation
import Alamofire


class BackendManager{
    func insertarPersona(nombre:String, apellido:String, telefono:String){
        
        let parametros = ["nombre": nombre, "apellido" : apellido, "telefono": telefono]
        
        let urlString = "https://proyecto1ios-2017a.mybluemix.net/insert"
        
        Alamofire.request(urlString, method: .post, parameters: parametros,
        encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                print(response)
        }
    }
    
    func obtenerUsuarios(){
        
        print("Entró a obtenerUsuarios")
        
        let urlString = "https://proyecto1ios-2017a.mybluemix.net/get";
        
        Alamofire.request(urlString).responseJSON { response in debugPrint(response)
            guard let json = response.result.value as? NSArray else {
                print("error")
                return
            }
            
            let usuariosArray = json
            //let usuariosArray = json["nombre"] as! NSArray
            //let usuariosDict = usuariosArray[0] as! NSDictionary
            
            NotificationCenter.default.post(name: NSNotification.Name("userList"), object: nil, userInfo: ["userList" : usuariosArray])
            
        }
        
    }
}

