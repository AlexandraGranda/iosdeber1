//
//  ListaUsuariosViewController.swift
//  Deber1
//
//  Created by Alexandra Granda on 7/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class ListaUsuariosViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var listaUsuariosTableView: UITableView!
    
    var userList:NSArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let bm = BackendManager()
        bm.obtenerUsuarios()
        NotificationCenter.default.addObserver(self, selector: #selector(actualizarDatos), name: NSNotification.Name("userList"), object: nil)
        
        // Do any additional setup after loading the view.
    }

    func actualizarDatos(_ notification:Notification)
    {
        userList = notification.userInfo?["userList"] as! NSArray
        
        DispatchQueue.main.async {
            self.listaUsuariosTableView.reloadData()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let userDict = userList[indexPath.row] as! NSDictionary
        cell.textLabel?.text = "\(userDict["nombre"]! ?? "") \(userDict["apellido"]! ?? "") - \(userDict["telefono"]! ?? "")"
        return cell
    }
	func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		return "Usuarios"
	}
}
